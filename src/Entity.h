//
// Created by sapphie on 02.03.19.
//

#ifndef GAMETEMPLATE_ENTITY_H
#define GAMETEMPLATE_ENTITY_H


#include <SFML/Graphics/Sprite.hpp>
#include "Board.h"
#include "Tools.h"

using namespace Tools;

class Entity {
public:
  const int x;
  const int y;
  const unsigned vel;
  const Direction facing;
  virtual sf::Sprite getSprite() const = 0;

protected:
  const bool moving;
  Entity(int _x, int _y, unsigned _vel, bool _moving, Direction _facing);

};


#endif //GAMETEMPLATE_ENTITY_H
