//
// Created by sapphie on 22.02.19.
//

#include <SFML/Window/Event.hpp>
#include <iostream>
#include "GameWindow.h"
#include "GameState.h"


std::map<sf::Keyboard::Key, Action> genKeyMap() {
  typedef std::pair<sf::Keyboard::Key, Action> pair;
  std::map<sf::Keyboard::Key, Action> mapper;

  // TODO qwerty this shit up
  // Insert all mappings
  mapper.insert(pair(sf::Keyboard::W, Action::move_up));
  mapper.insert(pair(sf::Keyboard::A, Action::move_left));
  mapper.insert(pair(sf::Keyboard::S, Action::move_down));
  mapper.insert(pair(sf::Keyboard::D, Action::move_right));

  mapper.insert(pair(sf::Keyboard::Up, Action::shoot_up));
  mapper.insert(pair(sf::Keyboard::Down, Action::shoot_down));
  mapper.insert(pair(sf::Keyboard::Left, Action::shoot_left));
  mapper.insert(pair(sf::Keyboard::Right, Action::shoot_right));

  return mapper;

}

const std::map<sf::Keyboard::Key, Action> GameWindow::key_to_action = genKeyMap();

GameWindow::GameWindow(std::string first_level_path) : sf::RenderWindow(sf::VideoMode::getDesktopMode(), "Death to the Orcs", sf::Style::Default | sf::Style::Resize) {
  m_max_framerate = 60;

  // Set texture
  m_sprite = sf::Sprite(m_texture);

  std::ifstream level_file(first_level_path);
  state = new GameState(level_file);
}

void GameWindow::run() {
  while (isOpen()) {
    if(state->finished) {
      close();
    }
    handleEvents();
    doThings();

    waitForFrame();
    // Draw all changes
    display();
  }
}

void GameWindow::handleEvents() {
  sf::Event e{};
  while (pollEvent(e)) {
    switch (e.type) {
      // Handle closing
      case sf::Event::Closed:
        close();
        break;
      case sf::Event::KeyReleased:
        if(e.key.code == sf::Keyboard::Escape) {
          close();
        }
        break;
      default:
        break;
    }
  }

  Action action = Action::no_action;
  GameState *oldState = state;
  for(const auto& key_action: key_to_action) {
    if(sf::Keyboard::isKeyPressed(key_action.first)) {
      action=action|key_action.second;
    }
  }

  state = new GameState(oldState->nextState(action));

  delete(oldState);

}

void GameWindow::doThings() {
  clear();
  draw(*state);
}

void GameWindow::onResize() {
  RenderWindow::onResize();
  const auto size = sf::Vector2f(state->size());
  const auto window_size = sf::Vector2f(getSize());
  auto camera_size = window_size;
  // Wider aspect ratio, we want to max out X
  if(size.x/size.y > window_size.x/window_size.y) {
    camera_size*=(size.x/window_size.x);
  } else {
    camera_size*=(size.y/window_size.y);
  }

  setView(sf::View(size/2.0f,camera_size));
}

void GameWindow::waitForFrame() {
  const auto frame_delay = sf::seconds(1.0f/m_max_framerate);
  const auto min_wait_length = sf::milliseconds(10);
  auto elapsed_time = last_frame.getElapsedTime();
  // Wait
  while(elapsed_time<frame_delay) {
    // If we need to wait a long duration, sleep
    if(elapsed_time>min_wait_length) {
      sf::sleep((frame_delay-elapsed_time)/2.0f);
    }

    elapsed_time = last_frame.getElapsedTime();
  }
  last_frame.restart();
}



