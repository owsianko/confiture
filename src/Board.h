//
// Created by sapphie on 02.03.19.
//

#ifndef GAMETEMPLATE_TILE_H
#define GAMETEMPLATE_TILE_H


#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <fstream>


class Board {
public:
  enum class Tile {
    active_good = 0, inactive_good, active_bad, inactive_bad, normal
  };

  Board(const Board& b) = default;

  explicit Board(std::ifstream& board_file);
  explicit Board(std::vector<std::vector<Tile>> tiles);

  const static sf::Texture* texture;
  sf::Sprite getSpriteAt(int x, int y) const;
  Board update(std::vector<std::pair<int,int>> to_update) const;

  unsigned width() const;
  unsigned height() const;
  unsigned pixelWidth() const;
  unsigned pixelHeight() const;


  const unsigned tile_side = 32;

private:
  const std::vector<std::vector<Tile>> tiles;
};


#endif //GAMETEMPLATE_TILE_H
