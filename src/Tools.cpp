//
// Created by sapphie on 02.03.19.
//
#include "Tools.h"
#include <SFML/Graphics/Rect.hpp>
#include <cmath>
#include <iostream>

namespace Tools {
  sf::IntRect getRect(int x, int y, int sprite_width, int space_btw_sprites) {
    //const int sprite_width = 32;
    int xCoord = space_btw_sprites + (sprite_width + space_btw_sprites) * x;
    int yCoord = space_btw_sprites + (sprite_width + space_btw_sprites) * y;
    return {xCoord, yCoord, sprite_width, sprite_width};
  }

  int clamp(int a, unsigned maxA) {
    if (a < 0) return 0;
    else if (a >= maxA) return maxA - 1;
    else return a;
  }

  sf::Vector2f d2v(Direction direction) {
    float sqrt2i = 1/sqrt2;
    switch (direction) {
      case Direction::up:
        return {0, -1};
      case Direction::down:
        return {0, 1};
      case Direction::left:
        return {-1,0};
      case Direction::right:
        return {1,0};
      case Direction::upleft:
        return {-sqrt2i, -sqrt2i};
      case Direction::downleft:
        return {-sqrt2i, sqrt2i};
      case Direction::upright:
        return {sqrt2i, -sqrt2i};
      case Direction::downright:
        return {sqrt2i, sqrt2i};
      case Direction::no_dir:
        return {0,0};
    }
  }

  Direction a2d(Action a, Action mask) {
    switch (a & mask) {
      case Action::shoot_up:
      case Action::move_up:
        return Direction::up;

      case Action::shoot_down:
      case Action::move_down:
        return Direction::down;

      case Action::shoot_left:
      case Action::move_left:
        return Direction::left;

      case Action::shoot_right:
      case Action::move_right:
        return Direction::right;

      case Action::move_upleft:
        return Direction::upleft;

      case Action::move_downleft:
        return Direction::downleft;

      case Action::move_upright:
        return Direction::upright;

      case Action::move_downright:
        return Direction::downright;

      default:
        return Direction::no_dir;
    }
  }

    Direction movementToDirection(sf::Vector2f movement) {
      float angle = atan2f(movement.y, movement.x);
      angle = angle/ ((float)M_PI) * 180;
      if( (0 >= angle && angle > -22.5) || (0 <angle && angle <= 22.5))
          return Direction::right;
      else if(angle > 22.5 && angle <= 67.5)
          return Direction::downright;
      else if(angle > 67.5 && angle <= 112.5)
          return Direction::down;
      else if(angle > 112.5 && angle <= 157.5)
          return Direction::downleft;
      else if((180 >= angle && angle > 157.5) || (-157.5>= angle && angle >= -180))
          return Direction::left;
      else if(angle > -157.5 && angle <= -112.5)
          return Direction::upleft;
      else if(angle > -112.5 && angle <= -67.5)
          return Direction::up;
      else if(angle > -67.5 && angle <= -22.5)
          return Direction::upright;
    }

  sf::Vector2f normalize(const sf::Vector2f& arg) {
    float norm = std::sqrt(arg.x*arg.x+arg.y*arg.y);
    if(norm < 1e-5) return arg;
    return {arg.x/norm, arg.y/norm};
  }
}
