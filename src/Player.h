//
// Created by sapphie on 26.02.19.
//

#ifndef GAMETEMPLATE_PLAYER_H
#define GAMETEMPLATE_PLAYER_H

#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Image.hpp>
#include "Entity.h"


class Player: public Entity {
public:
  Player(unsigned int _vel);


  sf::Sprite getSprite() const override;
  Player move(int dx, int dy, Direction direction, bool _shooting, const Board &) const;

  Player die() const;
  static const sf::Texture texture;

  const bool dead = false;
private:
  Player(Direction direction, int x, int y, int walk_frames, unsigned int vel, bool moving, bool shooting);

  explicit Player();

  static const unsigned int max_walk_frames = 3;
  static const unsigned player_side = 32;

  const int m_walk_frames;
  const bool m_shooting;
};


#endif //GAMETEMPLATE_PLAYER_H
