//
// Created by sapphie on 03.03.19.
//

#ifndef GAMETEMPLATE_PROJECTILE_H
#define GAMETEMPLATE_PROJECTILE_H


#include "Entity.h"

class Projectile : public Entity {
public:
  Projectile(int _x, int _y, unsigned _vel, Direction _dir);
  Projectile move(int dx, int dy) const;

  sf::Sprite getSprite() const override;

};


#endif //GAMETEMPLATE_PROJECTILE_H
