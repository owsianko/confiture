#include <iostream>
#include <SFML/Graphics.hpp>
#include "GameWindow.h"

int main() {
  GameWindow g("../levels/MAP1-jam.txt");
  g.run();
  return 0;
}
