//
// Created by sapphie on 26.02.19.
//

#include <SFML/Graphics/CircleShape.hpp>
#include <utility>
#include <iostream>
#include <SFML/Graphics/RectangleShape.hpp>
#include "GameState.h"
#include "Tools.h"

GameState::GameState(std::ifstream &board_file) : player(5), board(board_file), shot_cooldown(0), spawn_cooldown(500),
                                                  finished(false) {
}

void GameState::draw(sf::RenderTarget &renderTarget, sf::RenderStates states) const {

  sf::Sprite sprite = player.getSprite();
  // Draw tiles
  for (int i = 0; i < board.height(); i++) {
    for (int j = 0; j < board.width(); j++) {
      sf::Sprite tile = board.getSpriteAt(j, i);
      renderTarget.draw(tile);
    }
  }

  for (const auto &bloodstain: bloodstains) {
    sf::Sprite bloodstainSprite = bloodstain.getSprite();
    bloodstainSprite.setPosition(bloodstain.x, bloodstain.y);
    renderTarget.draw(bloodstainSprite);
  }

  // Draw projectiles
  for (const auto &proj: projectiles) {
    sf::CircleShape shape(5);
    shape.setFillColor(sf::Color::Red);
    shape.setPosition(proj.x, proj.y);
    renderTarget.draw(shape);
  }

  for (const auto &enemy: enemies) {
    sf::Sprite enemySprite = enemy.getSprite();
    enemySprite.setPosition(enemy.x, enemy.y);
    renderTarget.draw(enemySprite);
  }

  sprite.setPosition(player.x, player.y);
  renderTarget.draw(sprite);
}

Player GameState::getNextPlayer(Action a) const {
  int vel = player.vel;
  Direction moving_towards = a2d(a, Action::move_mask);
  sf::Vector2f movement_vector = d2v(moving_towards);
  int dx = static_cast<int>(vel * movement_vector.x);
  int dy = static_cast<int>(vel * movement_vector.y);
  Direction facing = moving_towards;
  if (facing == Direction::no_dir) {
    facing = Direction::down;
  }

  Direction shooting_towards = a2d(a, Action::shoot_mask);
  if (shooting_towards != Direction::no_dir) {
    facing = shooting_towards;
  }

  for (const auto &enemy: enemies) {
    if (enemy.x == player.x && enemy.y == player.y) {
      return player.die();
    }
  }
  // TODO hit detection (player detects if hit by enemies)

  return player.move(dx, dy, facing, false, board);
}

GameState GameState::nextState(Action a) const {
  // u ded lol
  if (player.dead) {
    return finish();
  }

  int next_shot_cooldown;
  int next_spawn_cooldown;
  if (shot_cooldown > 0) {
    next_shot_cooldown = shot_cooldown - 1;
  } else {
    if (a2d(a, Action::shoot_mask) != Direction::no_dir) {
      next_shot_cooldown = max_shot_cooldown;
    } else {
      next_shot_cooldown = 0;
    }
  }

  if (spawn_cooldown > 0) {
    next_spawn_cooldown = spawn_cooldown - 1;
  } else {
    next_spawn_cooldown = max_spawn_cooldown;
  }

  Board next_board = getNextBoard();
  return GameState(getNextPlayer(a), next_board, next_spawn_cooldown, next_shot_cooldown, getNextBloodstain(),
                   getNextEnemies(), getNextProjectiles(a),
                   false);
}

GameState::GameState(Player p, const Board &b, int _spawn_cooldown, int _shot_cooldown,
                     std::vector<Bloodstain> _bloodstains,
                     std::vector<Enemy> _enemies, std::vector<Projectile> _projectiles, bool _finished)
    :
    player(std::move(p)),
    board(b),
    bloodstains(std::move(_bloodstains)),
    projectiles(std::move(_projectiles)),
    enemies(std::move(_enemies)),
    shot_cooldown(_shot_cooldown),
    spawn_cooldown(_spawn_cooldown),
    finished(_finished) {
}

std::vector<Projectile> GameState::getNextProjectiles(Action a) const {
  std::vector<Projectile> next_projectiles;
  Direction shooting_towards = a2d(a, Action::shoot_mask);
  if (shooting_towards != Direction::no_dir) {
    if (shot_cooldown == 0) {
      next_projectiles.emplace_back(player.x, player.y, 7, shooting_towards);
    }
  }

  for (const auto &proj: projectiles) {
    sf::Vector2f dir = d2v(proj.facing);
    int dx = static_cast<int>(dir.x * proj.vel);
    int dy = static_cast<int>(dir.y * proj.vel);
    Projectile next_proj = proj.move(dx, dy);
    if (!(next_proj.x < -(int) (board.pixelWidth() / 2) ||
          next_proj.x > board.pixelWidth() * 3 / 2 ||
          next_proj.y < -(int) (board.pixelHeight() / 2) ||
          next_proj.y > board.pixelHeight() * 3 / 2)) {
      bool add = true;
      for (const auto &enemy: enemies) {
        // TODO improve
        add = add && !collisionDetected(proj, enemy);
      }
      if (add) {
        next_projectiles.emplace_back(next_proj);
      }

    }
  }
  return next_projectiles;
}

sf::Vector2u GameState::size() const {
  return {board.pixelWidth(), board.pixelHeight()};
}

std::vector<Enemy> GameState::getNextEnemies() const {
  std::vector<Enemy> next_enemies;
  if (spawn_cooldown == 0) {
    Enemy new_enemy(board.pixelWidth()/2, 0, Direction::down, 2);
    Enemy new_enemy2(board.pixelWidth()/2, board.pixelHeight(), Direction::down, 2);
    Enemy new_enemy3(0, board.pixelHeight()/2, Direction::down, 2);
    Enemy new_enemy4(board.pixelWidth(), board.pixelHeight()/2, Direction::down, 2);

    next_enemies.emplace_back(new_enemy);
    next_enemies.emplace_back(new_enemy2);
    next_enemies.emplace_back(new_enemy3);
    next_enemies.emplace_back(new_enemy4);
  }
  for (const auto &enemy: enemies) {
    auto movement = normalize(sf::Vector2f(player.x - enemy.x, player.y - enemy.y));
    auto vel = enemy.vel;
    auto next_enemy = enemy.move(static_cast<int>(vel * movement.x), static_cast<int>(vel * movement.y), movementToDirection(movement));
    bool add = true;
    for (const auto &proj: projectiles) {
      add = add && !collisionDetected(proj, enemy);
    }
    if (add) {
      next_enemies.emplace_back(next_enemy);
    }
  }
  return next_enemies;
}

std::vector<Bloodstain> GameState::getNextBloodstain() const {
  std::vector<Bloodstain> next_bloodstains;

  next_bloodstains.reserve(bloodstains.size());
  for (const auto &blood: bloodstains) {
    next_bloodstains.emplace_back(blood);
  }
  for (const auto &proj: projectiles) {
    for (const auto &enemy: enemies) {
      if (collisionDetected(proj, enemy)) {
        Bloodstain new_bloodstain(enemy.x, enemy.y);
        next_bloodstains.emplace_back(new_bloodstain);
      }
    }
  }
  return next_bloodstains;
}

bool GameState::collisionDetected(const Projectile &p, const Enemy &e) const {
  return (p.x + 2.5 > e.x &&
          p.x + 2.5 < e.x + e.enemy_side &&
          p.y + 2.5 > e.y &&
          p.y + 2.5 < e.y + e.enemy_side);
}

Board GameState::getNextBoard() const {
  std::vector<std::pair<int, int>> tiles_to_update;
  for (const auto &bloodstain: bloodstains) {
    std::vector<int> x_possib;
    std::vector<int> y_possib;

    int x_rem = bloodstain.x % board.tile_side;
    int y_rem = bloodstain.y % board.tile_side;

    x_possib.emplace_back(bloodstain.x / board.tile_side);
    y_possib.emplace_back(bloodstain.y / board.tile_side);
    if (x_rem != 0) {
      x_possib.emplace_back(bloodstain.x / board.tile_side + 1);
    }
    if (y_rem != 0) {
      y_possib.emplace_back(bloodstain.y / board.tile_side + 1);
    }
    for (int x : x_possib) {
      for (int y : y_possib) {
        tiles_to_update.emplace_back(std::pair<int, int>(x, y));
      }
    }
  }

  return board.update(tiles_to_update);
}
