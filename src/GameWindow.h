//
// Created by sapphie on 22.02.19.
//

#ifndef GAMETEMPLATE_GAMEWINDOW_H
#define GAMETEMPLATE_GAMEWINDOW_H


#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <map>
#include "GameState.h"

class GameWindow : public sf::RenderWindow {
public:
  explicit GameWindow(std::string);

  void run();
  static const std::map<sf::Keyboard::Key, Action> key_to_action;

protected:

  void handleEvents();
  void doThings();
  void waitForFrame();
  void onResize() override;

  sf::Clock last_frame;
  int m_max_framerate;
  sf::Sprite m_sprite;
  sf::Texture m_texture;
  GameState *state;
};


#endif //GAMETEMPLATE_GAMEWINDOW_H
