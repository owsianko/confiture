//
// Created by sapphie on 26.02.19.
//

#ifndef GAMETEMPLATE_GAMESTATE_H
#define GAMETEMPLATE_GAMESTATE_H


#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Window.hpp>
#include <fstream>
#include "Player.h"
#include "Board.h"
#include "Projectile.h"
#include "Tools.h"
#include "Enemy.h"
#include "Bloodstain.h"


using namespace Tools;

class GameState : public sf::Drawable {
private:
  const Player player;
  const std::vector<Projectile> projectiles;
  const std::vector<Enemy> enemies;
  const std::vector<Bloodstain> bloodstains;
  const Board board;
  const int shot_cooldown;
  const unsigned max_shot_cooldown = 30;
  const int spawn_cooldown;
  const unsigned max_spawn_cooldown = 300;

  GameState(Player p, const Board &b, int _spawn_cooldown, int _shot_cooldown, std::vector<Bloodstain> _bloodstains,
              std::vector<Enemy> _enemies, std::vector<Projectile> _projectiles, bool finished = false);

  inline GameState finish() const {
    return GameState(player, board, 0, 0, std::vector<Bloodstain>(), enemies, projectiles,
                     true);
  }

public:
  const bool finished;

  explicit GameState(std::ifstream &board_file);

  GameState(const GameState &g) = default;

  void draw(sf::RenderTarget &renderTarget, sf::RenderStates states) const override;

  GameState nextState(Action a) const;

  Player getNextPlayer(Action a) const;

  std::vector<Projectile> getNextProjectiles(Action a) const;

  std::vector<Enemy> getNextEnemies() const;

  std::vector<Bloodstain> getNextBloodstain() const;

  bool collisionDetected(const Projectile& p, const Enemy& e) const;

  Board getNextBoard() const;

  sf::Vector2u size() const;
};


#endif //GAMETEMPLATE_GAMESTATE_H
