//
// Created by sapphie on 03.03.19.
//

#include <SFML/Graphics/Image.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <iostream>
#include "Enemy.h"
#include "Tools.h"

//TODO: add direction as variable
Enemy::Enemy(int _x, int _y, Direction direction, unsigned _vel):
    Entity(_x,_y,_vel,true,direction){
}

sf::Image genEnemyImage() {
    sf::Image im;
    im.loadFromFile("../sprites/orc.png");
    return im;
}

sf::Texture genEnemyTexture() {
    sf::Texture texture;
    sf::Image img = genEnemyImage();
    texture.loadFromImage(img);
    return texture;
}

const sf::Texture Enemy::texture = genEnemyTexture();

sf::Sprite Enemy::getSprite() const {
    sf::Sprite res;
    res.setTexture(texture);
    const int spriteSize = 1280;
    switch (facing) {
        case Direction::upleft:
        case Direction::upright:
        case Direction::up:
            res.setTextureRect(Tools::getRect(1, 0, spriteSize, 1));
            break;
        case Direction::downleft:
        case Direction::downright:
        case Direction::down:
            res.setTextureRect(Tools::getRect(0, 0, spriteSize, 1));
            break;
        case Direction::left:
            res.setTextureRect(Tools::getRect(1, 1, spriteSize, 1));
            break;
        case Direction::right:
            res.setTextureRect(Tools::getRect(0, 1, spriteSize, 1));
            break;
    }
    float scale = static_cast<float>(enemy_side) / spriteSize;
    res.setScale(scale, scale);
  return res;
}

Enemy Enemy::move(int dx, int dy, Direction direction) const {
  return {x+dx, y+dy, direction,  vel};
}
