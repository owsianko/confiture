//
// Created by sapphie on 03.03.19.
//

#ifndef GAMETEMPLATE_ENEMY_H
#define GAMETEMPLATE_ENEMY_H

#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Image.hpp>
#include "Entity.h"

class Enemy: public Entity {
public:

  Enemy(int _x, int _y, Direction direction, unsigned _vel);

  sf::Sprite getSprite() const override;

  static const sf::Texture texture;

  Enemy move(int dx, int dy, Direction direction) const;
  static const unsigned enemy_side = 32;

private:


};


#endif //GAMETEMPLATE_ENEMY_H
