#include <utility>

//
// Created by sapphie on 02.03.19.
//

#include <stdexcept>
#include <iostream>
#include "Board.h"
#include "Tools.h"

sf::Image genBoardImage() {
  sf::Image im;
  im.loadFromFile("../sprites/cases_sol.png");
  return im;
}

sf::Texture genBoardTexture() {
  sf::Texture texture;
  sf::Image img = genBoardImage();
  texture.loadFromImage(img);
  return texture;
}

const sf::Texture* Board::texture = new sf::Texture(genBoardTexture());
sf::Sprite Board::getSpriteAt(int x, int y) const {
  sf::Sprite sprite;
  sprite.setTexture(*texture);
  const int spriteSize = 1279;
  switch (tiles[y][x]) {
    case Tile::active_good:
      sprite.setTextureRect(Tools::getRect(1, 1, spriteSize, 1));
      break;
    case Tile::inactive_good:
      sprite.setTextureRect(Tools::getRect(0, 1, spriteSize, 1));
      break;
    case Tile::active_bad:
      sprite.setTextureRect(Tools::getRect(0, 0, spriteSize, 1));
      break;
    case Tile::inactive_bad:
      sprite.setTextureRect(Tools::getRect(0, 0, spriteSize, 1));
      break;
    case Tile::normal:
      sprite.setTextureRect(Tools::getRect(1, 0, spriteSize, 1));
      break;
  }

  float scale = static_cast<float>(tile_side) / spriteSize;
  sprite.setScale(scale, scale);
  sprite.setPosition(x * tile_side, y * tile_side);
  return sprite;
}


unsigned Board::height() const {
  return static_cast<unsigned int>(tiles.size());
}

unsigned Board::width() const {
  if (!tiles.empty()) {
    return static_cast<unsigned int>(tiles[0].size());
  } else {
    throw std::runtime_error("fuck you");
  }
}

unsigned Board::pixelWidth() const {
  return width() * tile_side;
}

unsigned Board::pixelHeight() const {
  return height() * tile_side;
}

std::vector<std::vector<Board::Tile>> from_file(std::ifstream &board_file) {
  std::vector<std::vector<Board::Tile>> result;
  if (board_file.is_open()) {
    std::string line;
    while (getline(board_file, line)) {

      std::vector<Board::Tile> row;
      for (char c: line) {
        Board::Tile read_tile;
        switch (c) {
          case 'T':
          case '#':
            read_tile = Board::Tile::inactive_bad;
            break;
          case '.':
            read_tile = Board::Tile::normal;
            break;
          case 'X':
            read_tile = Board::Tile::inactive_good;
            break;
          default:
            throw std::invalid_argument("Invalid character, " + c);
        }
        row.push_back(read_tile);
      }
      result.push_back(row);
    }
    // Defensive af programming
    if (result.empty()) {
      throw std::invalid_argument("Empty document");
    }
    size_t expected_size = result[0].size();
    int i = 0;
    for (const auto &row: result) {
      if (row.size() != expected_size) {
        throw std::invalid_argument("unexpected number of characters on line" + i);
      }
      i++;
    }

    return result;
  } else {
    throw std::invalid_argument("Could not open file");
  }
}

Board::Board(std::ifstream &board_file) : tiles(from_file(board_file)) {
  texture = new sf::Texture(genBoardTexture());
}


Board::Board(std::vector<std::vector<Board::Tile>> _tiles) : tiles(std::move(_tiles)){

}

Board Board::update(std::vector<std::pair<int, int>> to_update) const {
  auto new_tiles = tiles;
  for (const auto &pair : to_update) {
    int x = pair.first;
    int y = pair.second;
    if (new_tiles[y][x] == Tile::inactive_good) {
      new_tiles[y][x] = Tile::active_good;
    } else if (new_tiles[y][x] == Tile::inactive_bad) {
      new_tiles[y][x] = Tile::active_bad;
    }
  }
  return Board(new_tiles);
}

