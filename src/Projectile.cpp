//
// Created by sapphie on 03.03.19.
//

#include "Projectile.h"

Projectile::Projectile(int _x, int _y, unsigned _vel, Direction _dir) :
    Entity(_x, _y, _vel, true, _dir) {
}

sf::Sprite Projectile::getSprite() const {
  // TODO
  return sf::Sprite();
}

Projectile Projectile::move(int dx, int dy) const {
  return {x+dx, y+dy, vel, facing};
}
