//
// Created by sapphie on 26.02.19.
//

#include <SFML/Graphics/Image.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <iostream>
#include "Player.h"
#include "Tools.h"

sf::Image genImage() {
  sf::Image im;
  im.loadFromFile("../sprites/magician.png");
  return im;
}

sf::Texture genTexture() {
  sf::Texture texture;
  sf::Image img = genImage();
  texture.loadFromImage(img);
  return texture;
}

const sf::Texture Player::texture = genTexture();

Player::Player(unsigned _vel) :
    m_walk_frames(0),
    m_shooting(false),
    Entity(750, 440, _vel, false, Direction::down) {}

sf::Sprite Player::getSprite() const {
  sf::Sprite res;
  res.setTexture(texture);
  const int spriteSize = 1280;
  switch (facing) {
    case Direction::upleft:
    case Direction::upright:
    case Direction::up:
      res.setTextureRect(Tools::getRect(1, 0, spriteSize, 1));
      break;
    case Direction::downleft:
    case Direction::downright:
    case Direction::down:
      res.setTextureRect(Tools::getRect(0, 0, spriteSize, 1));
      break;
    case Direction::left:
      res.setTextureRect(Tools::getRect(1, 1, spriteSize, 1));
      break;
    case Direction::right:
      res.setTextureRect(Tools::getRect(0, 1, spriteSize, 1));
      break;
  }
  float scale = static_cast<float>(player_side) / spriteSize;
  res.setScale(scale, scale);
  return res;
}

Player::Player(Direction direction, int x, int y, int walk_frames, unsigned vel, bool moving, bool shooting) :
    m_walk_frames(walk_frames),
    Entity(x, y, vel, moving, direction),
    m_shooting(shooting){
}

Player Player::move(int dx, int dy, Direction direction, bool _shooting, const Board &motherboard) const {
  int next_walkframes;
  if (moving) {
    next_walkframes = m_walk_frames - 1;
  } else {
    next_walkframes = max_walk_frames;
  }
  int next_x = Tools::clamp(x + dx, (int) motherboard.pixelWidth()-player_side);
  int next_y = Tools::clamp(y + dy, (int) motherboard.pixelHeight()-player_side);
  return {direction, next_x, next_y, next_walkframes, vel, true, _shooting};

}

Player Player::die() const {
  return Player();
}

Player::Player() : m_shooting(0), m_walk_frames(0), Entity(0,0,0,false,Direction::no_dir), dead(true) {

}
