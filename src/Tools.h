//
// Created by sapphie on 02.03.19.
//

#ifndef GAMETEMPLATE_TOOLS_H
#define GAMETEMPLATE_TOOLS_H

#include <SFML/Graphics/Rect.hpp>

namespace Tools {
  enum class Action {
    no_action = 0 << 0,
    move_up = 1 << 0,
    move_down = 1 << 1,
    move_left = 1 << 2,
    move_right = 1 << 3,
    move_upleft = (int) move_up | move_left,
    move_downleft = (int) move_left | move_down,
    move_upright = (int) move_up | move_right,
    move_downright = (int) move_down | move_right,
    move_mask = (int) (move_up | move_down | move_right | move_left),
    shoot_up = 1 << 4,
    shoot_down = 1 << 5,
    shoot_left = 1 << 6,
    shoot_right = 1 << 7,
    shoot_mask = (int) (shoot_up | shoot_down | shoot_left | shoot_right),
  };

  inline Action operator|(Action a, Action b) {
    return static_cast<Action>(static_cast<int>(a) | static_cast<int>(b));
  }

  inline Action operator&(Action a, Action b) {
    return static_cast<Action>(static_cast<int>(a) & static_cast<int>(b));
  }

  enum class Direction {
    no_dir, up, down, left, right, upleft, downleft, upright, downright,
  };

  Direction a2d(Action a, Action mask);

  sf::Vector2f d2v(Direction dir);

  sf::IntRect getRect(int x, int y, int sprite_width, int space_btw_sprites);
  
  int clamp(int a, unsigned maxA);

  const float sqrt2 = 1.414;

  sf::Vector2f normalize(const sf::Vector2f& arg);

  Direction movementToDirection(sf::Vector2f movement);
}

#endif //GAMETEMPLATE_TOOLS_H
