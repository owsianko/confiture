//
// Created by anne on 03.03.19.
//

#include <SFML/Graphics/Image.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <iostream>
#include "Bloodstain.h"
#include "Tools.h"

Bloodstain::Bloodstain(int _x, int _y):
        Entity(_x,_y,0,false,Direction::no_dir){
}

sf::Image genBloodstainImage() {
    sf::Image im;
    im.loadFromFile("../sprites/blood.png");
    return im;
}

sf::Texture genBloodstainTexture() {
    sf::Texture texture;
    sf::Image img = genBloodstainImage();
    texture.loadFromImage(img);
    return texture;
}

const sf::Texture Bloodstain::texture = genBloodstainTexture();

sf::Sprite Bloodstain::getSprite() const {
    sf::Sprite res;
    res.setTexture(texture);
    const int spriteSize = 1280;

    res.setTextureRect(Tools::getRect(0, 0, spriteSize, 1));

    float scale = static_cast<float>(bloodstain_side) / spriteSize;
    res.setScale(scale, scale);
    return res;
}
