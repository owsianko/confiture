//
// Created by anne on 03.03.19.
//

#ifndef GAMETEMPLATE_BLOODSTAIN_H
#define GAMETEMPLATE_BLOODSTAIN_H

#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Image.hpp>
#include "Entity.h"

class Bloodstain :public Entity {

public:
    Bloodstain(int _x, int _y);

    sf::Sprite getSprite() const override;

    static const sf::Texture texture;

private:
    static const unsigned bloodstain_side = 32;
};


#endif //GAMETEMPLATE_BLOODSTAIN_H
